#include <vector>
#include <deque>

class Convolver {

 private:
  std::vector<long int> h;
  std::deque<long int> x;
  
 public:
  Convolver(const std::vector<long int>& kernel);
  long int process(const long int& input_value);
  long int empty();
  
};

