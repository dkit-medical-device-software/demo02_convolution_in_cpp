Convolution example
===================

This is a brief example to demonstrate how a convolution machine is built in C++.
You should run it to make sure that you're able to compile and run C++ programs.
To do so, will need the CMake build system, so install it from your package manager first.
