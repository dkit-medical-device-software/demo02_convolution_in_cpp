#include "Convolver.h"

using namespace std;

Convolver::Convolver(const vector<long int>& kernel) {
  h = kernel;
  for ( int k = 0 ; k < h.size() ; ++k ) {
    x.push_front(0);
  }
}

long int Convolver::process(const long int& x_n) {
  x.pop_back();
  x.push_front(x_n);
  int y = 0;
  for ( int k = 0 ; k < h.size() ; ++k ) {
    y += h[k] * x[k];
  }
  return y;
}

long int Convolver::empty() {
  return process(0);
}
