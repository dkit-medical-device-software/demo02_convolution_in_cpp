#include "gtest/gtest.h"
#include "Convolver.h"
#include <vector>

namespace {

  class ConvolverTest : public ::testing::Test {
  protected:
    ConvolverTest() {
      // You can do set-up work for each test here.
    }
    
    virtual ~ConvolverTest() {
      // You can do clean-up work that doesn't throw exceptions here.
    }
    
    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:
    
    virtual void SetUp() {
      // Code here will be called immediately after the constructor (right
      // before each test).
    }
    
    virtual void TearDown() {
      // Code here will be called immediately after each test (right
      // before the destructor).
    }
    
    // Objects declared here can be used by all tests in the test case for Foo.
  };  

  TEST_F(ConvolverTest, NullSystemReturnsZero) {
    std::vector<long int> h, x;
    x.push_back(3);
    x.push_back(7);
    h.push_back(0);
    Convolver c(h);
    long int y = c.process(x[0]);
    ASSERT_EQ(0, y);
  }

  TEST_F(ConvolverTest, IdentitySystemReturnsZero) {
    std::vector<long int> h, x;
    x.push_back(3);
    x.push_back(7);
    h.push_back(1);
    Convolver c(h);
    long int y = c.process(x[0]);
    ASSERT_EQ(x[0], y);
  }

  TEST_F(ConvolverTest, DelayWorks) {
    std::vector<long int> h, x;
    x.push_back(3);
    x.push_back(7);
    h.push_back(0);
    h.push_back(1);
    Convolver c(h);
    c.process(x[0]);
    long int y = c.process(x[1]);
    ASSERT_EQ(x[0], y);
  }
}
