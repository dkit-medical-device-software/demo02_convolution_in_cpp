#include <vector>
#include <iostream>
#include "Convolver.h"

using namespace std;

int main(int argc, char** argv) {

  vector<long int> h;
  h.push_back(0);
  h.push_back(0);
  h.push_back(1);
  Convolver convolver(h);

  vector<long int> x;
  x.push_back(1);
  x.push_back(2);
  x.push_back(7);
  x.push_back(3);

  for ( int k = 0 ; k < x.size() ; ++k ) {
    cout << x[k] << " " << convolver.process(x[k]) << endl;
  }

  // show the emptying out
  for ( int k = 0 ; k < h.size()-1 ; ++k ) {
    cout << "? " << convolver.empty() << endl;
  }
  
  return 0;
  
}
